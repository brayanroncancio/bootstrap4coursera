$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
  });
  $('.carousel').carousel({
    interval: 3000
  })

  $('#modalregister').on('show.bs.modal', function (e) {
    console.log("El modal comienza abrirse");
    $('#btnregister').prop('disabled',true);
    $('#btnregister').removeClass('btn-outline-light');
    $('#btnregister').addClass('btn-outline-danger');
  })
  $('#modalregister').on('shown.bs.modal', function (e) {
    console.log("El modal terminó de abrir");
  })
  $('#modalregister').on('hide.bs.modal', function (e) {
    console.log("El modal comienza a ocultar");
  })
  $('#modalregister').on('hidden.bs.modal', function (e) {
    console.log("El modal terminó de ocultarse");
    $('#btnregister').prop('disabled',false);
    $('#btnregister').removeClass('btn-outline-danger');
    $('#btnregister').addClass('btn-outline-light');
  })